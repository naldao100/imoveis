import React from 'react';
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';
import MobileStepper from '@material-ui/core/MobileStepper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const styles = {
  root: {
    position: 'relative',
    // maxWidth: 400,
    flexGrow: 1,
  },
  slide: {
    padding: 15,
    minHeight: 100,
    color: '#fff',
  },
  slide1: {
    backgroundColor: '#FEA900',
  },
  slide2: {
    backgroundColor: '#B3DC4A',
  },
  slide3: {
    backgroundColor: '#6AC0FF',
  }
};

class Noticias extends React.Component {
  state = {
    index: 0,
  };

  handleChangeIndex = index => {
    this.setState({
      index,
    });
  };

  render() {
    const { index } = this.state;
    return (
      <div style={styles.root}>
          <AutoPlaySwipeableViews index={index} onChangeIndex={this.handleChangeIndex} interval={5000} enableMouseEvents>
            {
              this.props.feed.map((feed, index) => {
                let urlImg = Object.values(feed.enclosure);
                return (
                  <Grid container spacing={4} key={index}>
                    <Grid item lg={8} md={10} xl={8} xs={12}>
                      <Grid item xs={12} >
                        <Typography variant="h1" component="h2"  color="textSecondary" gutterBottom>
                          {feed.title}
                        </Typography>
                      </Grid>
                      <Grid item xs={12}>
                        <Typography color="textSecondary" align="justify">
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vel fermentum nunc, 
                          ac tristique sem. In condimentum consequat eros at feugiat. Donec sit amet ullamcorper metus. 
                          Curabitur vel elit fermentum, venenatis nulla a, hendrerit neque. Mauris tincidunt quam erat,
                           vitae maximus nisi luctus id. Ut convallis ut leo euismod tincidunt. Duis in sem eget velit 
                           egestas lacinia. Vestibulum eleifend dapibus est, at dignissim ex volutpat egestas. 
                           Etiam ut vehicula elit, in aliquam nulla. Nam bibendum augue enim, condimentum pharetra est euismod in. 
                           Ut scelerisque tempus urna et porttitor.
                        </Typography>
                      </Grid>
                    </Grid>
                    <Grid item lg={4} md={6} xl={4} xs={12}>
                      <Grid item xs={12}>
                        <img src={`${urlImg[0].url}`} style={{width: '100%'}}/>
                      </Grid>
                    </Grid>
                  </Grid>
                )
              })
            }
          </AutoPlaySwipeableViews>
      </div>
    );
  }
}

export default Noticias;
