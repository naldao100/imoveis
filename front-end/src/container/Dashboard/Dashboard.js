import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import moment from 'moment';
import Resources from '../../resources';
import Skeleton from '@material-ui/lab/Skeleton';
import Noticias from './Noticias/Noticias';
import {isEmpty} from 'lodash'

const feed = Resources("feed");
const listar = Resources("listar");
const climaTempo = Resources("climaTempo");
const listaDash = Resources("listaDash");

const skeletonCharge = [1,2,3]

const useStyles = makeStyles((theme) => ({
  root: {
    '& svg': {
      margin: theme.spacing(1.5),
    },
    '& hr': {
      margin: theme.spacing(0, 0.5),
    },
    
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    fontSize: 30,
    textTransform: 'uppercase'
  },
  appBar: {
    top: 'auto',
    bottom: 0,
  },
  cards:{
    minHeight: 243,
    marginBottom: 51
  },
  content: {
    textAlign: 'center',
    marginTop: 30
  },
  numero: {
    fontSize: 100,
    marginTop: 50
  },
  grid: {
    paddingBottom: 50
  }
}));

export default function Dashboard() {
  const classes = useStyles();
  const [hora, setHora] = React.useState();
  const [gestao, setGestao] = React.useState([]);
  const [rss, setRss] = React.useState([]);
  const [tempo, setTempo] = React.useState([]);

  const getGestao = () => {
    listaDash.fetchAll().then((response) => {
      console.log(response.data.dashboard);
      let data = response.data.dashboard;
      setGestao(data)
    })
  }

  const getClima = () => {
    climaTempo.fetchAll().then((response) => {
      let clima = response.data.tempo;
      setTempo(clima);
    })
  }

  const getFeed = () => {
    feed.fetchAll().then((response) => {
      let rss = response.data.feed.channel.item;
      setRss(rss);
    })
  }

  React.useEffect(() => {
    setInterval(() => {
      setHora(moment().format('H:mm'))
    }, 1000)
    getClima()
    getFeed()
    getGestao()
  }, []); 


  return (
    <div className={classes.root}>
      <Grid container spacing={4}>
        <Grid item lg={4} md={6} xl={4} xs={12}>
          <Grid item xs={12} className={classes.grid}>
            {
              (gestao.length === 0) &&
              skeletonCharge.map((value, index) => {
                return (
                  <Card className={classes.cards} key={index}>
                    <CardContent className={classes.content}>
                      <Typography className={classes.title} color="textSecondary" gutterBottom>
                        <Skeleton variant="text" width="50%" style={{marginLeft: 125}}/>
                      </Typography>
                      <Typography variant="h1" component="h2" gutterBottom className={classes.numero}>
                        <Skeleton variant="text" width="50%" style={{ marginLeft: 125}}/>
                      </Typography>
                    </CardContent>
                  </Card>
                )
              })
            }
            {
              (gestao.length > 0) &&
              gestao.map((value, index) => {
                return (
                   <Card className={classes.cards} key={index}>
                    <CardContent className={classes.content}>
                      <Typography className={classes.title} color="textSecondary" gutterBottom>
                        {value.titulo}
                    </Typography>
                      <Typography variant="h1" component="h2" gutterBottom className={classes.numero}>
                        {value.valor}
                    </Typography>
                    </CardContent>
                  </Card>
                )
              })
            }
          </Grid>
        </Grid>
        <Grid item lg={8} md={6} xl={8} xs={12}>
          <Grid item xs={12}>
            <Card className={classes.cards}>
              <CardContent className={classes.content} style={{ marginTop: 159, minHeight: 668 }}>
                {
                  (rss.length > 0) &&
                  <Noticias
                  feed={rss}
                  />
                }
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </Grid>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <Grid container alignItems="center" className={classes.root}>
            <Typography variant="h4" color="inherit">
              {hora}
            </Typography>
            <Divider orientation="vertical" flexItem />
            <Typography variant="h4" color="inherit">
              {
                (!isEmpty(tempo)) &&
                `${tempo.name} - ${tempo.data.temperature}°`
              }
            </Typography>
          </Grid>          
        </Toolbar>
      </AppBar>
    </div>
  );
}
