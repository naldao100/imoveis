import React, { forwardRef, useRef } from 'react';
import MaterialTable, { MTableToolbar, MTableEditRow } from 'material-table';
import {
  AddBox,
  ArrowUpward,
  Check,
  ChevronLeft,
  ChevronRight,
  Clear,
  DeleteOutline,
  Edit,
  FilterList,
  FirstPage,
  LastPage,
  Remove,
  SaveAlt,
  Search,
  ViewColumn
} from "@material-ui/icons";
import { Container, Divider, Typography, Button } from '@material-ui/core';
import './Home.scss';
import Resources from '../../resources';
import moment from 'moment';
 
const listar = Resources("listar");
const cadastro = Resources("cadastrar");
const atualizar = Resources("atualizar");
const deletar = Resources("deletar");

const tableIcons = {
  // tableIcons
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => (
    <ChevronRight {...props} ref={ref} />
  )),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => (
    <ChevronLeft {...props} ref={ref} />
  )),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};

export default function Home() {
  const tableRef = useRef();
  const [placeholder, setPlaceholder] = React.useState(0);
  const [data, setData] = React.useState();
  const [editable, setEditable] = React.useState();
  const [isLoading, setLoading] = React.useState(false);
  const [state, setState] = React.useState({
    columns: [
      { title: 'ID', field: 'id', editable: 'never' },
      { title: 'Título', field: 'titulo', validate: rowData => rowData.titulo === '' ? { isValid: false, helperText: 'Campo obrigatório' } : true},
      { title: 'Valor', field: 'valor', type: 'numeric', validate: rowData => rowData.valor === '' ? { isValid: false, helperText: 'Campo obrigatório' } : true },
      { title: 'Data/Hora', field: 'datetime', type: 'datetime', editable: 'never'},
    ]
  });

  const onStartList = () => {
    listar.fetchAll().then((response) => {
      let data = response.data.gestao_numeros;
      let newData = data.map((value) => {
        return {
          id: `#${value.id}`,
          idGestao: value.id,
          titulo: value.titulo,
          valor: value.valor,
          valor: value.valor,
          datetime: moment(value.created_at).format('DD/MM/YYYY h:m'),
        }
      })
      setLoading(false);
      setData(newData);
    }).catch((error) => {
      setLoading(false);
    })
  }

  const cadastrar = (data) => {
    setLoading(true);
    cadastro.create(data).then((response) => {
      onStartList();
    }).catch(() => {
      setLoading(false);
    })
  }

  const editar = (data) => {
    setLoading(true);
    atualizar.put(data.idGestao, data).then((response) => {
      onStartList();
    }).catch(() => {
      setLoading(false);
    })
  }

  const excluir = (data) => {
    setLoading(true);
    deletar.remove(data.idGestao).then((response) => {
      onStartList();
    }).catch(() => {
      setLoading(false);
    })
  }

  React.useEffect(() => {
    setLoading(true);
    onStartList()
  }, []);  
  
  const handleAddRow = () => {
    tableRef.current.state.showAddRow = true;
    setEditable({
        onRowAdd: newData =>
          new Promise((resolve, reject) => {
            setEditable();
            resolve();
          })
    });

  };
  const forceUpdate = () => {
    setPlaceholder(o => o + 1);
  }
  return (
    <Container maxWidth="xl" style={{paddingTop: 50}}>
      <MaterialTable
        tableRef={tableRef}
        data={data}
        icons={tableIcons}
        columns={state.columns}
        options={{
          actionsColumnIndex: -1,
          search: false,
          showTitle: false
        }}
        isLoading={isLoading}
        components={{
          Toolbar: props => (
            <div>
              <MTableToolbar {...props}/>
              <div style={{ padding: '0px 10px' }}>
                <Button
                  onClick={handleAddRow}
                  color="primary"
                  variant="contained"
                  style={{ marginRight: 20, float: 'right' }}
                  size="small"
                >
                  Novo Número
                </Button>
                <Button
                  color="primary"
                  variant="contained"
                  style={{ marginRight: 20, float: 'right' }}
                  size="small"
                  href="/dashboard"
                >
                  Ver Dashboard
                </Button>
                <Typography variant="h3" gutterBottom>
                  Gestão de Números
                </Typography>
              </div>
              <Divider variant="middle" style={{paddingTop: 2, marginTop: 20, marginBottom: 20}}/>
            </div>
          ),
          EditRow: props => {
            return (
              <MTableEditRow
                {...props}
                onEditingApproved={(mode, newData, oldData) => {
                  if (mode === 'add') {
                    cadastrar(newData);
                    tableRef.current.state.showAddRow = false;
                  } else if (mode === 'update') {
                    editar(newData)
                  } else {
                    excluir(newData)
                  }
                  forceUpdate();
                }}
              />
            );
          }
        }}
        editable={editable, {
          onRowAdd: newData =>
            new Promise((resolve, reject) => {
              setData([...state.data, newData]);
              setEditable();
              resolve();
            }),
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                if (oldData) {
                  setState((prevState) => {
                    const data = [...prevState];
                    data[data.indexOf(oldData)] = newData;
                    return { ...prevState, data };
                  });
                }
              }, 600);
            }),
          onRowDelete: (oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                setState((prevState) => {
                  const data = [...prevState];
                  data.splice(data.indexOf(oldData), 1);
                  return { ...prevState, data };
                });
              }, 600);
            }),
        }}
      />
    </Container>
  );
}
