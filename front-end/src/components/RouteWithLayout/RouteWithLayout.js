import React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import PropTypes from 'prop-types';
import { isAuthenticated } from '../../resources/auth';
const RouteWithLayout = props => {
  const { layout: Layout, component: Component, ...rest } = props;
  return (
    <Route
      {...rest}
      render={matchProps => (
        isAuthenticated() ? (
            <Layout>
              <Component {...matchProps} />
            </Layout>
        ) : (
            <Switch>
              <Layout>
                <Redirect
                  exact
                  from={props.path}
                  to="/login"
                />
                <Component {...matchProps} />
              </Layout>
            </Switch>
          )
      )}
    />
  );
};

RouteWithLayout.propTypes = {
  component: PropTypes.any.isRequired,
  layout: PropTypes.any.isRequired,
  path: PropTypes.string
};

export default RouteWithLayout;
