import http from './http'

export default class Resource {
  constructor (url) {
    this.url = url
  }

  fetch (id, query = {}) {
    return http.get(`${this.url}/${id}`, {params: query})
  }

  fetchAll (query = {}) {
    return http.get(this.url, {params: query})
  }

  create (data = {}, query = {}) {
    return http.post(this.url, data, {params: query})
  }

  put (id, data = {}, query = {}) {
    return http.put(`${this.url}/${id}`, data, {params: query})
  }

  remove (id, data = {}, query = {}) {
    return http({
      method: 'delete',
      url: `${this.url}/${id}`,
      data: data,
      params: query
    })
  }
  update (data = {}) {
    return http.put(this.url, data)
  }
}
