import Resource from './resource'

const instances = {}
const url = "http://127.0.0.1:8000/api/";
const appResources = {
  "listar": function () {
    return new Resource(`${url}gestao-numero/listar`);
  },
  "cadastrar": function () {
    return new Resource(`${url}gestao-numero/cadastrar`);
  },
  "atualizar": function () {
    return new Resource(`${url}gestao-numero/atualizar`);
  },
  "deletar": function () {
    return new Resource(`${url}gestao-numero/deletar`);
  },
  "login": function () {
    return new Resource(`${url}login`);
  },
  'logout': function () {
    return new Resource(`${url}logout`);
  },
  'climaTempo': function () {
    return new Resource(`${url}tempo`);
  },
  'feed': function () {
    return new Resource(`${url}feed`);
  },
  'listaDash': function () {
    return new Resource(`${url}dashboard/listar`);
  },
}

export default function get (name) {
  if (!instances[name]) {
    instances[name] = appResources[name]()
  }

  return instances[name]
}
