import Cookies from 'js-cookie';
import { isEmpty } from 'lodash';
import Resources from './index';
const logoutService = Resources("logout");
export const isAuthenticated = () => !isEmpty(Cookies.get('FAST_TOKEN'));
export const getToken = () => Cookies.get('FAST_TOKEN');
export const login = token => {
  Cookies.set('FAST_TOKEN', token.access_token, { expires: token.expires_in});
};
// export const logout = () => {
//   Cookies.remove('FAST_TOKEN');
// };

export const logout = () => {
  Cookies.remove('FAST_TOKEN');
  logoutService.create().catch((error) => {
    console.log(error);
  })
};
