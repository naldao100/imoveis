import axios from 'axios';
import Cookies from 'js-cookie';
import {isEmpty} from 'lodash'
const http = axios.create({
  baseURL: 'http://127.0.0.1:8000',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  },
})

const validate = (error) => {

  if (!isEmpty(error.response)) {    
    if (error.response.status === 401) {
      Cookies.remove('FAST_TOKEN');
      window.location = '/login'
    }
    
    if (error.response.status === 403) {
      window.history.back();
    }
  }
}

http.interceptors.request.use((config) => {
  
  const token = Cookies.get('FAST_TOKEN');

  if (token) {
    config.headers.Authorization = `Bearer ${token}`
  }
  return config
}, (error) => {
  validate(error)
  return Promise.reject(error)
})

http.interceptors.response.use((response) => {
  return response
}, (error) => {
  validate(error)
  return Promise.reject(error)
})

export default http
