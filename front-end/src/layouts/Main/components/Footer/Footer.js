import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Typography, Link } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(1),
    textAlign: 'center',
    display: 'flex',
    flexDirection: 'column',
    // minHeight: '93vh',
    // position: 'fixed',
    bottom: 0,
    left: 0,
    right: 0
  },
  footer: {
    padding: theme.spacing(3, 2),
    marginTop: 'auto',
  },
}));

const Footer = props => {
  const { className, ...rest } = props;

  const classes = useStyles();

  return (
    <div
      {...rest}
      className={clsx(classes.root, className)}
    >
      <footer className={classes.footer}>
        <Typography variant="body1">
          &copy;{' '}
            FastSale System - 2020
        </Typography>
        <Typography variant="caption">
          Criado e desenvolvido por: <Link component="a" href="#" target="_blank"> FastGroup Technology Development </Link> - Todos os direitos reservados
        </Typography>
      </footer>
    </div>
  );
};

Footer.propTypes = {
  className: PropTypes.string
};

export default Footer;
