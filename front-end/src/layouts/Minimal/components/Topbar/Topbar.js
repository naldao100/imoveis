import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { AppBar, Toolbar, Badge, Hidden, IconButton } from '@material-ui/core';
import { logout } from '../../../../resources/auth';
import InputIcon from '@material-ui/icons/Input';
import { isAuthenticated } from '../../../../resources/auth';
const useStyles = makeStyles((theme) => ({
  root: {
    boxShadow: 'none',
    backgroundColor: '#330099'
  },
  flexGrow: {
    flexGrow: 1
  },
  signOutButton: {
    marginLeft: theme.spacing(1)
  }
}));

const logoutSystem = () => {
  logout();
}

const Topbar = props => {
  const { className, ...rest } = props;

  const classes = useStyles();

  return (
    <AppBar
      {...rest}
      className={clsx(classes.root, className)}
      color="primary"
      position="fixed"
    >
      <Toolbar>
        <RouterLink to="/">
          <div>
            <span style={{ color: 'white', fontSize: 25, fontWeight: 'bold' }}>Verticais Imóveis</span>
          </div>
        </RouterLink>
        <div className={classes.flexGrow} />
        <Hidden mdDown>
          {
            (isAuthenticated()) &&
            <IconButton
              className={classes.signOutButton}
              color="inherit"
              onClick={logoutSystem}
            >
              <InputIcon />
            </IconButton>
          }
        </Hidden>
      </Toolbar>
    </AppBar>
  );
};

Topbar.propTypes = {
  className: PropTypes.string
};

export default Topbar;
