# Imóveis

Sistema para gerar números de imóveis vendidos, alugados e disponíveis

## BACK-END

O back-end do sistema foi desenvolvido com Laravel e esta rodando com docker

### Requisitos back-end

- Instalar docker (https://www.docker.com/get-started)
- Após a instalação do docker rodar os seguintes comandos dentro da pasta
do back-end do projeto
    - **docker-compose up** (aguarde finalizar o processo)
    - **docker-compose exec myapp php artisan migrate**
    - **docker-compose exec myapp php artisan db:seed**

## FRONT-END

O front-end do sistema esta desenvolvido em ReactJs e para executar o front-end basta
rodar os seguintes comandos dentro da pasta do front-end do projeto

- **npm install**
- **npm start**

Ao executar o npm start o sistema irá rodar em **localhost:3000**