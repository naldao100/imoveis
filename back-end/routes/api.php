<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'AuthController@login');

Route::group(['middleware' => ['auth.jwt']], function () {
    Route::post('logout', 'AuthController@logout');
    Route::get('feed', 'ConnectExternalController@getFeed');
    Route::get('tempo', 'ConnectExternalController@getTempo');
    Route::group(['prefix' => 'gestao-numero'], function () {
        Route::get('listar', 'GestaoNumerosController@index');
        Route::post('cadastrar', 'GestaoNumerosController@cadastrar');
        Route::put('atualizar/{id}', 'GestaoNumerosController@atualizar');
        Route::delete('deletar/{id}', 'GestaoNumerosController@deletar');
    });
    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('listar', 'GestaoNumerosController@listar');
        Route::post('cadastrar', 'GestaoNumerosController@cadastrar');
        Route::put('atualizar/{id}', 'GestaoNumerosController@atualizar');
        Route::delete('deletar/{id}', 'GestaoNumerosController@deletar');
    });
});
