<?php

namespace App\Helper;

use Illuminate\Support\Facades\Http;

trait ConnectExternal
{
    public static function getFeed()
    {
        $response = Http::get('https://exame.com/mercado-imobiliario/feed/', [
        ]);

        if (!$response->successful()) {
            throw new \Exception('Erro ao realizar a operação', 412);
        }
        return simplexml_load_string($response->body());
    }

    public static function getTempo()
    {
        $response = Http::get('http://apiadvisor.climatempo.com.br/api/v1/weather/locale/3477/current', [
            'token' => '324a95a45250a120f5bcb4a6f269f9f1'
        ]);

        if (!$response->successful()) {
            throw new \Exception('Erro ao realizar a operação', 412);
        }
        return $response->json();
    }
}
