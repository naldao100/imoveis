<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GestaoNumeros extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titulo', 'valor', 'created_at',
    ];
}
