<?php

namespace App\Http\Controllers;

use App\Helper\ConnectExternal;

class ConnectExternalController extends Controller
{
    use ConnectExternal;

    public function getFeed()
    {
        try {
            $feed = ConnectExternal::getFeed();
            return response()->json(['feed' => $feed], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getCode());
        }

    }

    public function getTempo()
    {
        try {
            $tempo = ConnectExternal::getTempo();
            return response()->json(['tempo' => $tempo], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getCode());
        }
    }
}
