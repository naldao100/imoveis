<?php

namespace App\Http\Controllers;

use App\GestaoNumeros;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GestaoNumerosController extends Controller
{
    public function index()
    {
        $gestao = GestaoNumeros::orderBy('id', 'DESC')->get();
        return response()->json(['gestao_numeros' => $gestao], 200);
    }

    public function listar()
    {
        $dashboard = GestaoNumeros::orderBy('id', 'DESC')->limit(3)->get();
        return response()->json(['dashboard' => $dashboard], 200);
    }

    public function cadastrar(Request $request)
    {
        try {
            $validator = Validator::make(
                $request->all(), [
                    'titulo' => 'required|max:255',
                    'valor' => 'required',
                ]
            );

            if ($validator->fails()) {
                throw new \Exception($validator->errors(), 412);
            }

            $gestao = new GestaoNumeros;

            $gestao->titulo = $request->titulo;
            $gestao->valor = $request->valor;

            $gestao->save();

            return response()->json(['gestao_numero' => $gestao], 201);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getCode());
        }
    }

    public function atualizar($id, Request $request)
    {
        try {
            $validator = Validator::make(
                $request->all(),
                [
                    'titulo' => 'required|max:255',
                    'valor' => 'required',
                ]
            );

            if ($validator->fails()) {
                throw new \Exception($validator->errors(), 412);
            }

            $gestao = GestaoNumeros::find($id);

            $gestao->titulo = $request->titulo;
            $gestao->valor = $request->valor;

            $gestao->save();

            return response()->json(['gestao_numero' => $gestao], 201);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getCode());
        }
    }

    public function deletar($id)
    {
        try {
            $gestao = GestaoNumeros::find($id);
            if (empty($gestao)) {
                throw new \Exception('Objeto não encontrado', 412);
            }

           $destroy = GestaoNumeros::destroy($id);

            return response()->json(['gestao_numero' => $destroy], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getCode());
        }

    }
}
