<?php

use App\User;
use App\GestaoNumeros;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        User::create([
            'email' => 'teste@teste.com',
            'password' => Hash::make('testeOrbital020989'), // password
            'remember_token' => Str::random(10),
            'email_verified_at' => now(),
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('gestao_numeros')->delete();
        DB::table('gestao_numeros')->insert([
            ['titulo' => 'Imóveis Diponíveis', 'valor' => 1.203, 'created_at' => new \DateTime('now')],
            ['titulo' => 'Imóveis Vendidos', 'valor' => 103, 'created_at' => new \DateTime('now')],
            ['titulo' => 'Imóveis Alugados', 'valor' => 32.193, 'created_at' => new \DateTime('now')],
        ]);
    }
}
